## 大赛资料

### 1. 参赛必读文档
- [quick-start](https://gitee.com/openharmony/docs/tree/master/zh-cn/application-dev/quick-start)
- [快速入门讲解](https://gitee.com/isrc_ohos/ultimate-harmony-reference/blob/master/OpenHarmony%20JS%20Demo%E5%BC%80%E5%8F%91%E8%AE%B2%E8%A7%A3.md)  
- [项目配置流程](https://gitee.com/isrc_ohos/ultimate-harmony-reference/blob/master/OpenHarmony%20JS%E9%A1%B9%E7%9B%AE%E5%BC%80%E5%8F%91%E6%B5%81%E7%A8%8B.md)

### 2. 参考 Demo 
- [JS Demo开发示例](https://gitee.com/isrc_ohos/open-harmony-js-demos)

### 3. 视频教程指引  
- [视频教程](http://mtw.so/6oLJOt)

### 4. 其他参考资料
- [Taurus AI Camera开发套件简介](https://gitee.com/gitee-community/openharmony_components/blob/master/赛事辅导锦囊/Taurus%20AI%20Camera开发套件简介.md)
- [单板说明](https://www.bilibili.com/video/BV1zQ4y1Z7fR)
- [配件介绍](https://www.bilibili.com/video/BV13v411j7Ab)
- [开箱组装演示](https://www.bilibili.com/video/BV1aV411J7tb)
- [码云仓环境搭建指南](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/quickstart-lite-steps-hi3516-setting.md)